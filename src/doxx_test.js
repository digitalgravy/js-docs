/**
 * Doxx Test Object is a test class
 * @constructor
 */
var Doxx_TestObject = function(){
  this.param_1 = true;
  this.param_2 = 'a';
  this.param_3 = 1;
};


/**
 * Get all parameters from the object
 * @return {Object} An object of parameters
 * @api public
 */
Doxx_TestObject.prototype.getParameters = function(){
  return {
    param_1: this.param_1,
    param_2: this.param_2,
    param_3: this.param_3
  };
};

/**
 * Get a single parameter from the object
 * @param {String} key The key for the parameter
 * @return {any} The parameter value
 * @api public
 */
Doxx_TestObject.prototype.getParameter = function(key){
  return this[key];
};

/**
 * Set parameter on the object
 * @param {String} key The key for the parameter
 * @param {any} val The value to assign to the parameter
 * @return {any} The new parameter value
 * @api public
 */
Doxx_TestObject.prototype.setParameter = function(key, val){
  return this[key] = val;
};
