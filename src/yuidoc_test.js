/**
* YUI Test Object is a test class
*
* @class YUI_TestObject
* @constructor
*/
var YUI_TestObject = function(){
  this.param_1 = true;
  this.param_2 = 'a';
  this.param_3 = 1;
};


/**
* Get all parameters from the object
*
* @method getParameters
* @return {Object} parameters A parameters object
*/
YUI_TestObject.prototype.getParameters = function(){
  return {
    param_1: this.param_1,
    param_2: this.param_2,
    param_3: this.param_3
  };
};

/**
* Get a single parameter from the object
*
* @method getParameter
* @param {String} key The key for the parameter
* @return {any} parameter A single parameter value
*/
YUI_TestObject.prototype.getParameter = function(key){
  return this[key];
};

/**
* Set all parameters from the object
*
* @method getParameters
* @param {String} key The key for the parameter
* @param {any} val The value to assign to the parameter
* @return {any} parameter The result of the parameter assignment
*/
YUI_TestObject.prototype.setParameter = function(key, val){
  return this[key] = val;
};
