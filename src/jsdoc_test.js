/**
* JSDoc Test Object is a test class
*
* @class JSDoc_TestObject
* @constructor
*/
var JSDoc_TestObject = function(){
  /** @type {boolean} */
  this.param_1 = true;
  /** @type {string} */
  this.param_2 = 'a';
  /** @type {number} */
  this.param_3 = 1;
};


/**
* Get all parameters from the object
*
* @return {Object} parameters A parameters object
*/
JSDoc_TestObject.prototype.getParameters = function(){
  return {
    param_1: this.param_1,
    param_2: this.param_2,
    param_3: this.param_3
  };
};

/**
* Get a single parameter from the object
*
* @param {String} key The key for the parameter
* @return {any} parameter A single parameter value
*/
JSDoc_TestObject.prototype.getParameter = function(key){
  return this[key];
};

/**
* Set all parameters from the object
*
* @param {String} key The key for the parameter
* @param {any} val The value to assign to the parameter
* @return {any} parameter The result of the parameter assignment
*/
JSDoc_TestObject.prototype.setParameter = function(key, val){
  return this[key] = val;
};
