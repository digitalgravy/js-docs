/**
 * Description
 * @method TestObject
 * @return 
 */
var TestObject = function(){
  this.param_1 = true;
  this.param_2 = 'a';
  this.param_3 = 1;
};

/**
 * Get all the parameters 
 * @method getParameters
 * @return ObjectExpression
 */
TestObject.prototype.getParameters = function(){
  return {
    param_1: this.param_1,
    param_2: this.param_2,
    param_3: this.param_3
  };
};

/**
 * Get a single parameter 
 * @method getParameter
 * @param {} key
 * @return MemberExpression
 */
TestObject.prototype.getParameter = function(key){
  return this[key];
};

/**
 * Set a single parameter 
 * @method setParameter
 * @param {} key
 * @param {} val
 * @return AssignmentExpression
 */
TestObject.prototype.setParameter = function(key, val){
  return this[key] = val;
};
