/*!
 * Doxx Test Object is a test class
 */
var Doxx_TestObject = function(){
  // docco only shows comments over here if they're inline.
  this.param_1 = true;
  this.param_2 = 'a';
  this.param_3 = 1;
  // in fact these comments are extracted in situe and placed here...
};


/*!
 * Get all parameters from the object
 */
Doxx_TestObject.prototype.getParameters = function(){
  return {
    param_1: this.param_1,
    param_2: this.param_2,
    param_3: this.param_3
  };
};

/*!
 * Get a single parameter from the object
 */
Doxx_TestObject.prototype.getParameter = function(key){
  return this[key];
};

/*!
 * Set all parameters from the object
 */
Doxx_TestObject.prototype.setParameter = function(key, val){
  return this[key] = val;
};
