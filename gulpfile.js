var gulp    = require('gulp'),
    docco   = require('gulp-docco'),
    doxx    = require('gulp-doxx'),
    jsdoc   = require('gulp-jsdoc'),
    yuidoc  = require('gulp-yuidoc'),
    exec    = require('gulp-exec'),
    rename  = require('gulp-rename');

gulp.task('docco', function(){
  gulp.src('./src/docco*.js')
      .pipe(docco())
      .pipe(gulp.dest('./docs/docco'));
});
gulp.task('doxx', function(){
  gulp.src('./src/doxx*.js')
      .pipe(doxx({
        title: 'Test App',
        urlPrefix: '/docs'
      }))
      .pipe(gulp.dest('./docs/doxx'));
});
gulp.task('jsdoc', function(){
  gulp.src('./src/jsdoc*.js')
      .pipe(jsdoc('./docs/jsdoc'));
});
gulp.task('yuidoc', function(){
  gulp.src('./src/yuidoc*.js')
      .pipe(yuidoc())
      .pipe(gulp.dest('./docs/yuidoc'));
});

gulp.task('docs', ['docco', 'doxx', 'jsdoc', 'yuidoc']);


/** EXPERIMENTAL **/
gulp.task('smartcomments', function(){
  gulp.src('./src/test.js')
      .pipe(rename('SC_test.js'))
      .pipe(gulp.dest('./src'))
      .pipe(exec('smartcomments -g -t ./src/SC_test.js'));
});